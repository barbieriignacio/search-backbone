/**
 * compare
 * Compares a variable with a value
 * Usage: {{#compare variable "operator" "value"}} ... {{else}} ... {{/compare}}
 * Example: {{#compare role "==" "sender"}} ... {{else}} ... {{/compare}}
 */

Handlebars.registerHelper('compare', function (lvalue, operator, rvalue, options) {

    var operators, result;

    if (arguments.length < 3) {
        throw new Error("Handlerbars Helper 'compare' needs 2 parameters");
    }

    if (options === undefined) {
        options = rvalue;
        rvalue = operator;
        operator = "===";
    }

    operators = {
        '==': function (l, r) { return l == r; },
        '===': function (l, r) { return l === r; },
        '!=': function (l, r) { return l != r; },
        '!==': function (l, r) { return l !== r; },
        '<': function (l, r) { return l < r; },
        '>': function (l, r) { return l > r; },
        '<=': function (l, r) { return l <= r; },
        '>=': function (l, r) { return l >= r; },
        'typeof': function (l, r) { return typeof l == r; },
        'notContained': function (l, r) { return !operators['contained'](l,r);},
        'contained': function (l, r) {
            var result = false;
            if ( l && r && l.length > 0 && r.length > 0 ){
                var lVal;
                if (typeof(l) == "string"){
                    lVal = l.split(',');
                } else {
                    lVal = l;
                }

                var rVal;
                if (typeof(r) == "string"){
                    rVal = r.split(',');
                } else {
                    rVal = r;
                }

                for (var i=0; i<lVal.length && !result; i++) {
                    for (var j=0; j<rVal.length && !result; j++) {
                        result = lVal[i] === rVal[j];
                    }
                }
            }
            return result;
        }
    };

    if (!operators[operator]) {
        throw new Error("Handlerbars Helper 'compare' doesn't know the operator " + operator);
    }

    result = operators[operator](lvalue, rvalue);

    if (result) {
        return options.fn(this);
    } else {
        return options.inverse(this);
    }

});

/**
 * loggedUser
 * Return an attribute of the logged user
 * Usage: {{loggedUser "name"}}
 */
Handlebars.registerHelper('loggedUser', function(options) {
    // var attr = arguments[0];

    // if (!attr) {
    //     throw new Error('You must send the attribute that you want to get.');
    // }
    
    // if(Chat.utils.user){
    //     return Chat.utils.user[attr];
    // }else{
    //     return null;
    // }
});

/**
 * Date Format (datetime)
 * Converts UNIX Epoch time to DD/MM/YYYY HH:MM:SS
 * 1431406539023 -> 12/05/2015 01:55:39
 * Usage: {{dateFormatTime yourDate}}
 */
Handlebars.registerHelper('dateFormatTime', function(context) {
    var date = new Date(context),
        myReturn = "";

    myReturn += date.getDate() < 10 ? ('0' + date.getDate()) : date.getDate();
    myReturn += '/';
    myReturn += (date.getMonth() + 1) < 10 ? ('0' + (date.getMonth() + 1)) : date.getMonth() + 1;
    myReturn += '/';
    myReturn += date.getFullYear();
    myReturn += ' ';
    myReturn += date.getHours() < 10 ? ('0' + date.getHours()) : date.getHours();
    myReturn += ':';
    myReturn += date.getMinutes() < 10 ? ('0' + date.getMinutes()) : date.getMinutes();
    myReturn += ':';
    myReturn += date.getSeconds() < 10 ? ('0' + date.getSeconds()) : date.getSeconds();

    return myReturn;
});