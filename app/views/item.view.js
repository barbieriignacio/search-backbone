Search.Views.Item = Backbone.View.extend({
    tagName: 'ol',

    className: 'items',  

    template: __templates.item.main,

    events: {
        "click .image": "loadMoreInfo",
        "click .thumbail": "loadBigImage",
        "click .buy-now": "loadBuyInfo"
    },

    initialize: function () {
        var that = this;
        this.selectedId = null;

        if (this.collection.models.length > 0) {
            this.render();    
        }

         this.collection.on('add', function () {
            that.render();
        });
    },

    render: function () {
        this.$el.html(this.template(this.collection.toJSON()));
    },

    loadMoreInfo : function (event) {
        event.preventDefault();
        var currentId = event.currentTarget.parentNode.getAttribute('data-item');

        if (this.selectedId === currentId) {
            this.$el.children().removeClass('active');
            Backbone.history.navigate('/', { trigger: true });

        } else {
            // Load the next view
            Backbone.history.navigate('details/'+currentId, { trigger: true });
            this.$el.children().removeClass('active');
            // Add active class
            event.currentTarget.parentNode.className += ' active';
        }
    },

    loadBigImage : function (event) {
        var srcImage = event.currentTarget.src;

        this.$el.find('.img-big').attr('src', srcImage);
    },    

    loadBuyInfo: function () {
        // Load the next view
        Backbone.history.navigate('buy/'+currentId, { trigger: true });
    }
});