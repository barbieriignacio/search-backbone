Search.Views.Details = Backbone.View.extend({
    tagName: 'div',

    className: 'details',  

    template: __templates.details.main,

    events: {
        "click .buy-now": "loadBuyInfo"
    },

    initialize: function () {
        this.render();
    },

    render: function () {
        this.$el.html(this.template(this.model.toJSON()));
    },

    loadBuyInfo: function (event) {
        var currentId = event.currentTarget.getAttribute('data-id');

        // Load the next view
        Backbone.history.navigate('buy/'+currentId, { trigger: true });
    }
});