Search.Views.Buy = Backbone.View.extend({
    tagName: 'div',

    className: 'buy',  

    template: __templates.buy.main,

    events: {
    },

    initialize: function () {
        this.render();
    },

    render: function () {
        this.$el.html(this.template(this.collection.toJSON()));
    }
});