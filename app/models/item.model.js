Search.Models.Item = Backbone.Model.extend({
    defaults: {
        'id': null,
        'type': null,
        'title': null,
        'price': null,
        'image': null,
        'link': null,
        'description': null,
        'freeShipping': null,
        'imagen1': null,
        'imagen2': null,
        'imagen3': null
    }
});