Search.Models.Buy = Backbone.Model.extend({
    defaults: {
        'id': null,
        'price': null,
        'installments': null,
        'paymentMetohd': null
    }
});