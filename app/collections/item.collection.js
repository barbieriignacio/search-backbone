Search.Collections.Items = Backbone.Collection.extend({
    'url': '/items',
    model: Search.Models.Item
});