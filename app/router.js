var Router = Backbone.Router.extend({
    routes: {
        "" : "index",
        "details/:id" : "details",
        "buy/:id" : "buy"
    },

    index: function () {
        if (Search.Cache.items === undefined) {
            Search.Cache.items = new Search.Collections.Items();
        
            Search.Cache.items.fetch();
        }

        Search.Cache.Views.item = new Search.Views.Item({
                collection: Search.Cache.items
            });

        $('#app').html(Search.Cache.Views.item.el);
    },

    details: function (id) {
        if (Search.Cache.items === undefined) {
            Search.Cache.items = new Search.Collections.Items();
            Search.Cache.items.fetch();
            
            Search.Cache.Views.item = new Search.Views.Item({
                collection: Search.Cache.items
            });
        }

        if (id !== Search.Cache.Views.item.selectedId) {
            if (Search.Cache.Views.details !== undefined) {
                // Delet open view
                Search.Cache.Views.details.remove();          
            }
        
            // Create clicked view
            Search.Cache.Views.details = new Search.Views.Details({
                    model: Search.Cache.items.get(id)
                });

            Search.Cache.Views.item.selectedId = id;
            Search.Cache.Views.item.$('[data-item='+id+']').append(Search.Cache.Views.details.el);

        } else {
            console.log('back');
        }
    },

    buy: function (id) {
        // Create buy view
        Search.Cache.Views.buy = new Search.Views.Buy({
                model: new Search.Models.Buy()
            });
    }

});

$(function(){
    new Router();

    Backbone.history.start({
        pushState: true
    });
});