$.mockjax({
    url: "/items",
    proxy: "mocks/responses/item-get.json"
});

$.mockjax({
    url: "/details/:id",
    proxy: "mocks/responses/details-get.json"
});

$.mockjax({
    url: "/buy/:id",
    proxy: "mocks/responses/buy-get.json"
});


// Disable Mocks
// $.mockjax.clear();